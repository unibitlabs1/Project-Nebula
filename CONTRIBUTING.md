Steps for creating good issues or pull requests.
Links to external documentation, mailing lists, or a code of conduct.
Community and behavioral expectations.

https://help.github.com/articles/creating-a-pull-request/

https://opensource.guide/starting-a-project/

^^ For reference..


Do you like planning events?
    Organize workshops or meetups about the project, like @fzamperin did for NodeSchool (complete)
    Organize the project’s conference (if they have one)
    Help community members find the right conferences and submit proposals for speaking (complete)

Do you like to design?
    Restructure layouts to improve the project’s usability (complete)
    Conduct user research to reorganize and refine the project’s navigation or menus, like Drupal suggests (in progress)
    Put together a style guide to help the project have a consistent visual design (in progress)
    Create art for t-shirts or a new logo, like hapi.js’s contributors did (complete)

Do you like to write?
    Write and improve the project’s documentation (in progress)
    Curate a folder of examples showing how the project is used (in progress)
    Start a newsletter for the project, or curate highlights from the mailing list (in progress)
    Write tutorials for the project, like PyPA’s contributors did (in progress)
    Write a translation for the project’s documentation (complete)
    
